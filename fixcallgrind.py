import re
import sys
import subprocess

callgrind_file = sys.argv[1]
executable_file = sys.argv[2]

if len(sys.argv) < 3:
    print("Usage: fix-callgrind.py <callgrind file> <executable>")
    sys.exit(1)

# Read the symbols
symbols_data = subprocess.getoutput(f"nm {executable_file}").splitlines()
symbol_regex = r"([0-9a-f]+)\s.\s(.+)"
symbols = {}
for line in symbols_data:
    match = re.match(symbol_regex, line)
    if match:
        symbols[match.group(1)] = match.group(2)

# Read the callgrind file
with open(callgrind_file) as f:
    callgrind = f.read()

# Replace the symbols in the callgrind file
for key in symbols:
    callgrind = callgrind.replace("0x" + key, symbols[key])

# Write back
with open(callgrind_file, "w") as f:
    f.write(callgrind)
